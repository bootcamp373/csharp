﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
static void Main(string[] args)
{
   
    }


namespace NameParser
{
    public class NameParsing
    {
        public static void GetName(out string Name, out string firstName, out string lastName, out string middleName)
        {
            Console.Write("Enter a full name: ");
            Name = Console.ReadLine();
            Name = Name.ToUpper();
            firstName = Name.Substring(0, Name.IndexOf(" "));

            lastName = Name.Substring(Name.LastIndexOf(" "));
            if (Name.IndexOf(" ") == Name.LastIndexOf(" "))
            {
                middleName = "";
            }
            else
            {
                middleName = Name.Substring(Name.IndexOf(" ") + 1, Name.LastIndexOf(" ") - Name.IndexOf(" "));
            };

        }
    }
    }

