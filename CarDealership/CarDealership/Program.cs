﻿using System;
using System.IO;
using System.Runtime.ConstrainedExecution;

namespace CarDealership;
public class Program
{
    static void Main(string[] args)
    {
        string fileName = @"C:\Academy\AddressLabel\CarDealership\Cars.txt";
        string outputFile = @"C:\Academy\AddressLabel\CarDealership\CarReceipt.txt";

        StreamReader inputFile = null;
        string[,] cars = new string[5, 6];
        string[] car;
        int carNums = 0;
        int carId = 0;
        int carselection = 0;
        string buyanswer;
        try
        {
            inputFile = new StreamReader(fileName);
            while (!inputFile.EndOfStream)
            {
                string line = inputFile.ReadLine();
                car = ReadLine(line);
                for (int i = 0; i < car.Length; i++)
                {
                 cars[carNums, i] = car[i];
                }
                carNums++;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(
            $"Error opening file: {ex.Message}");
        }
        finally
        {
            if (inputFile != null) inputFile.Close();
        }

        //Console.Write("{0,4}", "");
        for (int k = 0; k < cars.GetLength(1); k++)
        {
         Console.Write("{0,10}", k);
        }
        Console.Write(Environment.NewLine);
        Console.Write("{0,4}", "");

        for (int m = 0; m < cars.GetLength(1); m++)
        {
            Console.Write("{0,10}", "---");
        }
        Console.Write(Environment.NewLine);
        for (int i = 0; i < cars.GetLength(0); i++)
        {
            Console.Write($"{i} | ");
            for (int j = 0; j < cars.GetLength(1); j++)
            {

                Console.Write("{0,10}", cars[i, j]);
            }
            Console.Write(Environment.NewLine + Environment.NewLine);
        }
        Console.Write("Select a car (enter a number 1-5)");
        carselection = Convert.ToInt32(Console.ReadLine());


        //carselection = Console.ReadLine();

        switch (carselection)
        {
            case 1:
                {
                    //Console.Write("{0,10}", cars);
                    Console.Write("VIN:" + " " +     cars[0, 0] + " ");
                    Console.Write("Make:" + " " +    cars[0, 1] + " ");
                    Console.Write("Model:" + " " +   cars[0, 2] + " ");
                    Console.Write("Color:" + " " +   cars[0, 3] + " ");
                    Console.Write("Mileage:" + " " + cars[0, 4] + " ");
                    Console.Write("Price:" + " " +   cars[0, 5] + " ");




                    break;
                }
            case 2:
                {
                    Console.Write("VIN:" + " " +     cars[1, 0] + " ");
                    Console.Write("Make:" + " " +    cars[1, 1] + " ");
                    Console.Write("Model:" + " " +   cars[1, 2] + " ");
                    Console.Write("Color:" + " " +   cars[1, 3] + " ");
                    Console.Write("Mileage:" + " " + cars[1, 4] + " ");
                    Console.Write("Price:" + " " +   cars[1, 5] + " ");
                    break;
                }
            case 3:
                {
                    Console.Write("VIN:" + " "     + cars[2, 0]);
                    Console.Write("Make:" + " "    + cars[2, 1]);
                    Console.Write("Model:" + " "   + cars[2, 2]);
                    Console.Write("Color:" + " "   + cars[2, 3]);
                    Console.Write("Mileage:" + " " + cars[2, 4]);
                    Console.Write("Price:" + " "   + cars[2, 5]);


                    break;
                }
            case 4:
                {

                    Console.Write("VIN:" + " "     + cars[3, 0] + " " );
                    Console.Write("Make:" + " "    + cars[3, 1] + " ");
                    Console.Write("Model:" + " "   + cars[3, 2] + " ");
                    Console.Write("Color:" + " "   + cars[3, 3] + " ");
                    Console.Write("Mileage:" + " " + cars[3, 4] + " ");
                    Console.Write("Price:" + " "   + cars[3, 5] + " ");


                    break;
                }
            case 5:
                {
                    Console.Write("VIN:" + " "     + cars[4, 0] + " ");
                    Console.Write("Make:" + " "    + cars[4, 1] + " ");
                    Console.Write("Model:" + " "   + cars[4, 2] + " ");
                    Console.Write("Color:" + " "   + cars[4, 3] + " ");
                    Console.Write("Mileage:" + " " + cars[4, 4] + " ");
                    Console.Write("Price:" + " "   + cars[4, 5] + " ");


                    break;
                }
            default:
                {
                    Console.WriteLine($"{carselection} product not found");
                    break;
                }

              

        }
        Console.WriteLine("   ");

        Console.WriteLine("Enter BUY to purchase or Main to return");
        buyanswer = Console.ReadLine();

        if (buyanswer == "Buy" || buyanswer == "BUY" || buyanswer == "buy") {
            Console.WriteLine("You have selected Make - {0} Model - {1} Color - {2} Mileage - {3} Price - ${4} to purchase.", cars[carselection - 1, 1], cars[carselection - 1, 2], cars[carselection - 1, 3], cars[carselection - 1, 4], cars[carselection - 1, 5]);
            try { 
            using (StreamWriter outputfile = new StreamWriter(outputFile, true))
            {
                outputfile.WriteLine("New car receipt!");
                outputfile.WriteLine("--------------------------------");
                outputfile.WriteLine("Make : " + cars[carselection, 1]);
                outputfile.WriteLine("Model: " + cars[carselection, 2]);
                outputfile.WriteLine("Color: " + cars[carselection, 3]);
                outputfile.WriteLine("Miles: " + cars[carselection, 4]);
                outputfile.WriteLine("Price: " + cars[carselection, 5]);
            }
            }
            catch (Exception ex) {
                Console.WriteLine($"Error opening file: {ex.Message}");


                }

        }
        else
        {
            return;
        }

    }

    public static string[] ReadLine(string line)
        {
            Console.WriteLine(line);
            //comma delimiter
            String[] car = line.Split(',');
            return car;
        }

    




    }
