﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FileProcessing
{
    public class TimeCard
    {
        public string Name { get; set; }
        public decimal HoursWorked { get; set; }
        public decimal PayRate { get; set; }

        public static TimeCard CreateTimeCard(string inputLine)
        {
            TimeCard timeCard = new TimeCard();
            //pipe delimited
            string[] fields = inputLine.Split("|");
            try
            {
                timeCard.Name = fields[0];
                timeCard.HoursWorked = Convert.ToDecimal(fields[1]);
                timeCard.PayRate = Convert.ToDecimal(fields[2]);
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine("index out of range exception " + ex.Message);
            }
            catch (NullReferenceException ex1)
            {
                Console.WriteLine("null reference exception " + ex1.Message);
            }
            catch (OverflowException ex2)
            {
                Console.WriteLine("Overflow Exception " + ex2.Message);
            }
            catch (Exception ex3)
            {
                Console.WriteLine("Error : " + ex3.Message);
            }

            return timeCard;
        }

        public decimal GetGrossPay()
        {
            decimal GrossPay = 0;
            decimal overtimeHours = 0;
            decimal regularHours = 0;
            if (this.HoursWorked > 40)
            {
                overtimeHours = this.HoursWorked - 40;
            }
            //multiply regular rate by 1.5 - standard OT
            GrossPay = overtimeHours * (this.PayRate * 1.5M) + regularHours * this.PayRate;
            return GrossPay;
        }
    }
}
