﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperheroCapstone.Models;
using SuperHeroCapstone.Models;
using Microsoft.Data.SqlClient;

namespace SuperHeroCapstone.Controllers

{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperheroController : ControllerBase
    {
        private readonly SuperheroContext _context;
        //private readonly SuperheroManager superheroManager;

        public SuperheroController(SuperheroContext context)
        {
            _context = context;
        }
        // POST api/books
        [HttpPost]
        public ActionResult Post(Superhero superhero)
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = SuperheroDb; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Superhero> _superhero = new List<Superhero>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "INSERT INTO Superhero(Id, Name, Nickname, Superpower, Telephone, Dateofbirth) VALUES(" + superhero.Id + ",'" + superhero.Name + "','" + superhero.Nickname + "','" +  superhero.Superpower + "','" + superhero.Telephone + "','" + superhero.Dateofbirth + "')";
            //string query = "INSERT INTO Superhero(Id) VALUES(" + superhero.Id + ")";

            SqlCommand cmd = new SqlCommand(query, conn);

            int rowsChanged = cmd.ExecuteNonQuery();
            Console.WriteLine($"{rowsChanged} rows changed");

            conn.Close();


            if (superhero.Dateofbirth < DateTime.Today)

            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _superhero.Add(superhero);
                string location = $"api/Superhero/{superhero.Id}";
                return Created(location, superhero);
            }
            return BadRequest("Invalid data"); // Status 400
        }



        [HttpGet]
        public ActionResult<Superhero> Get()
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = SuperheroDb; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Superhero> _superhero = new List<Superhero>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "SELECT * FROM Superhero";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Superhero superhero = new Superhero();
                superhero.Id = (int)reader[0];
                superhero.Name = (string)reader[1];
                superhero.Nickname = (string)reader[2];
                superhero.Superpower = (string)reader[3];
                superhero.Telephone = (string)reader[4];
                superhero.Dateofbirth = (DateTime)reader[5];
                _superhero.Add(superhero);
            }
            reader.Close();
            conn.Close();
            return Ok(_superhero);
        }

        // GET api/superhero/1 (return superhero where id=1)
        [HttpGet("{Id}")]
        public ActionResult<Superhero> Get(int Id)
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = SuperheroDb; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Superhero> _superhero = new List<Superhero>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "SELECT * FROM Superhero WHERE Id = @Id";

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            SqlDataReader reader = cmd.ExecuteReader();
            Superhero superhero = new Superhero();
            while (reader.Read())
            {
                superhero.Id = (int)reader[0];
                superhero.Name = (string)reader[1];
                superhero.Nickname = (string)reader[2];
                superhero.Superpower = (string)reader[3];
                superhero.Telephone = (string)reader[4];
                superhero.Dateofbirth = (DateTime)reader[5];
            }

            reader.Close();
            conn.Close();
            return Ok(superhero);
        }

        [HttpGet("{Superpower}")]
        public ActionResult <Superhero> get(string Superpower)
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = SuperheroDb; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Superhero> _superhero = new List<Superhero>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "SELECT * FROM Superhero WHERE Superpower = @Superpower";

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Superpower", Superpower);
            SqlDataReader reader = cmd.ExecuteReader();
            Superhero superhero = new Superhero();
            while (reader.Read())
            {
                superhero.Id = (int)reader[0];
                superhero.Name = (string)reader[1];
                superhero.Nickname = (string)reader[2];
                superhero.Superpower = (string)reader[3];
                superhero.Telephone = (string)reader[4];
                superhero.Dateofbirth = (DateTime)reader[5];
            }

            reader.Close();
            conn.Close();
            return Ok(superhero);
        }

        [HttpDelete("{Id}")]
        public ActionResult Delete(int Id)
        {
            var dbContext = new SuperheroContext();
            string location = "";
            Superhero superheroDeleted = dbContext.Superhero.FirstOrDefault(b => b.Id == Id);

            Console.WriteLine(Id);

            if (superheroDeleted == null)
            {
                return NotFound();
            }
            else
            {
                dbContext.Remove(superheroDeleted);
                dbContext.SaveChanges();
                return Ok();
            }
        }

        [HttpPut("{Id}")]
        public ActionResult Put(Superhero superhero, int Id)
        {
            var dbContext = new SuperheroContext();
            string location = "";
            Superhero superheroUpdated = dbContext.Superhero.FirstOrDefault(b => b.Id == Id);

            if (superheroUpdated == null)
            {
                dbContext.Add(superhero);

                dbContext.SaveChanges();
                location = $"api/Superhero/{superhero.Id}";
                return Created(location, superhero);
            }
            else
            {
                // execute put
                superheroUpdated.Name = superhero.Name;
                superheroUpdated.Nickname = superhero.Nickname;
                superheroUpdated.Superpower = superhero.Superpower;
                superheroUpdated.Telephone = superhero.Telephone;
                superheroUpdated.Dateofbirth = superhero.Dateofbirth;

                dbContext.SaveChanges();
                return Ok();
            }
        }

    }
}