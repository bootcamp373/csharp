﻿using Microsoft.EntityFrameworkCore;
using SuperHeroCapstone.Models;

namespace SuperheroCapstone.Models
{
    public class SuperheroContext : DbContext
    {
        public SuperheroContext()
        {
        }

        public SuperheroContext(DbContextOptions<SuperheroContext> options) : base(options)
        {

        }

        public DbSet<Superhero> Superhero { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
         optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=SuperheroDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }
    }
}
