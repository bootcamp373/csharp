﻿namespace SuperHeroCapstone.Models
{
    public class Superhero
    {   
        public int Id { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Superpower { get; set; }
        public string Telephone { get; set; }
        public DateTime Dateofbirth { get; set; }

    }
}
