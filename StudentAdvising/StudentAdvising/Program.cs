﻿
string command = "";
//double returnValueDbl = 0.0;
//string returnValueString = "";
string advisingLocation = "";

while (command != "QUIT")
{
    //returnValueDbl = 0.0;
    command = "";
    advisingLocation = "";

    Console.WriteLine("Command for Student Advising Program -- 'QUIT' in all caps to exit -->");
    Console.WriteLine(" Student: Student Advising");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();
    if (command == "Student")
    {
        try
        {
            GetData(out string inputStudentName, out string inputStudentMajor, out string inputStudentClassification);
            advisingLocation = GetAdvisingLocation(ref inputStudentMajor, inputStudentClassification);
            Console.WriteLine("Advising for {3} who is a {0} {1} majors is located at : {2}", inputStudentMajor, inputStudentClassification, advisingLocation, inputStudentName);
        }
        catch (Exception x)
        {
            Console.WriteLine($"Error: {x.Message}");
        }
    }

    else if (command != "QUIT")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
}



static void GetData(out string inputStudentName, out string inputStudentMajor, out string inputStudentClassification)
{
    Console.Write("What is your name? ");
    inputStudentName = Console.ReadLine();
    Console.Write("What is the your major? ");
    inputStudentMajor = Console.ReadLine();
    Console.Write("What is the your classification (class year - Freshman, Sophomore, Junior, Senior)? ");
    inputStudentClassification = Console.ReadLine();
}
static string GetAdvisingLocation(ref string Major, string Classification)
{
    string location = "";
    if (Major != null && Classification != null)
    {
        if (Major == "BIOL" && (Classification == "Freshman" || Classification == "Sophomore"))
        {
            location = "Science Bldg, Room 310";
        }
        else if (Major == "BIOL" && (Classification == "Junior" || Classification == "Senior"))
        {
            location = "Science Bldg, Room 311";
        }
        else if (Major == "CSCI")
        {
            location = "Sheppard Hall, Room 314";
        }
        else if (Major == "ENG" && (Classification == "Freshman"))
        {
            location = "Kerr Hall, Room 201";
        }
        else if (Major == "ENG" && (Classification == "Junior" || Classification == "Senior" || Classification == "Sophomore"))
        {
            location = "Kerr Hall, Room 312";
        }
        else if (Major == "HIST")
        {
            location = "Kerr Hall, Room 114";
        }
        else if (Major == "MKT" && (Classification == "Freshman" || Classification == "Sophomore" || Classification == "Junior"))
        {
            location = "Westly Hall, Room 310";
        }
        else if (Major == "MKT" && (Classification == "Senior"))
        {
            location = "Westly Hall, Room 313";
        }
    }
    if (Major != null)
    {
        switch (Major)
        {
            case "ENG":
                Major = "English";
                break;
            case "BIOL":
                Major = "Biology";
                break;
            case "CSCI":
                Major = "Computer Science";
                break;
            case "HIST":
                Major = "History";
                break;
            case "MKT":
                Major = "Marketing";
                break;
            default:
                Major = "No Major Declared";
                break;
        }
    }

    return location;
}