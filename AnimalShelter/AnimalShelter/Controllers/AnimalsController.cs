﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AnimalShelter.Models;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace AnimalShelter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalsController : ControllerBase
    {


        // POST api/books
        [HttpPost]
        public ActionResult Post(Animal animal)
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = AnimalShelter; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Animal> _animal = new List<Animal>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "INSERT INTO Animal(Id, Name, Species, Breed, Owner, CheckInDate, CheckOutDate) VALUES(" + animal.Id + ",'" + animal.Name + "','" + animal.Species + "','" + animal.Breed + "','" + animal.Owner + "','" + animal.CheckInDate + "','" + animal.CheckOutDate + "')";

            SqlCommand cmd = new SqlCommand(query, conn);

            int rowsChanged = cmd.ExecuteNonQuery();
            Console.WriteLine($"{rowsChanged} rows changed");

            conn.Close();


            if (animal.CheckInDate < DateTime.Today)

            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _animal.Add(animal);
                string location = $"api/Animal/{animal.Id}";
                return Created(location, animal);
            }
            return BadRequest("Invalid data"); // Status 400
        }

       

        [HttpGet]
        public ActionResult<Animal> Get()
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = AnimalShelter; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Animal> _animal = new List<Animal>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "SELECT * FROM Animal";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Animal animal = new Animal();
                animal.Id = (int)reader[0];
                animal.Name = (string)reader[1];
                animal.Species = (string)reader[2];
                animal.Breed = (string)reader[3];
                animal.Owner = (string)reader[4];
                animal.CheckInDate = (DateTime)reader[5];
                animal.CheckOutDate = (DateTime)reader[6];
                 _animal.Add(animal);
            }
            reader.Close();
            conn.Close();
            return Ok(_animal);
        }

        // GET api/books/1 (return book where id=1)
        [HttpGet("{Id}")]
        public ActionResult<Animal> Get(int Id)
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = AnimalShelter; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Animal> _animal = new List<Animal>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "SELECT * FROM Books WHERE BookId = @BookId";

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", Id);
            SqlDataReader reader = cmd.ExecuteReader();
            Animal animal = new Animal();
            while (reader.Read())
            {
                animal.Id = (int)reader[0];
                animal.Name = (string)reader[1];
                animal.Species = (string)reader[2];
                animal.Breed = (string)reader[3];
                animal.Owner = (string)reader[4];
                animal.CheckInDate = (DateTime)reader[5];
                animal.CheckOutDate = (DateTime)reader[6];
            }

            reader.Close();
            conn.Close();
            return Ok(animal);
        }

        [HttpDelete("{Id}")]
        public ActionResult Delete(int Id)
        {
            var dbContext = new AnimalContext();
            string location = "";
            Animal animalDeleted = dbContext.Animal.FirstOrDefault(b => b.Id == Id);

            Console.WriteLine(Id);

            if (animalDeleted == null)
            {
                return NotFound();
            }
            else
            {
                dbContext.Remove(animalDeleted);
                dbContext.SaveChanges();
                return Ok();
            }
        }

        [HttpPut("{Id}")]
        public ActionResult Put(Animal animal, int Id)
        {
            var dbContext = new AnimalContext();
            string location = "";
            Animal animalUpdated = dbContext.Animal.FirstOrDefault(b => b.Id == Id);

            if (animalUpdated == null)
            {
                dbContext.Add(animal);

                dbContext.SaveChanges();
                location = $"api/Books/{animal.Id}";
                return Created(location, animal);
            }
            else
            {
                // execute put
                animalUpdated.Name = animal.Name;
                animalUpdated.Species = animal.Species;
                animalUpdated.Breed = animal.Breed;
                animalUpdated.Owner = animal.Owner;
                animalUpdated.CheckInDate = animal.CheckInDate;
                animalUpdated.CheckOutDate = animal.CheckOutDate;

                dbContext.SaveChanges();
                return Ok();
            }
        }











        //private readonly AnimalContext _context;

        //public AnimalsController(AnimalContext context)
        //{
        //    _context = context;
        //}

        // GET: api/Animals
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Animal>>> GetProducts()
        //{
        //    if (_context.Animals == null)
        //    {
        //        return NotFound();
        //    }
        //    return await _context.Animals.ToListAsync();
        //}

    }
}
