﻿
string bContinue = "Y";
while (bContinue == "Y")
{
    try
    {
        Console.Write("How much are you borrowing?: ");

        string inputStringAmountBorrowed = Console.ReadLine();
        double amountBorrowed = Convert.ToDouble(inputStringAmountBorrowed);
        Console.WriteLine(amountBorrowed);

        Console.Write("What is your interest rate?");
        string inputStringAnnualInterestRate = Console.ReadLine();
        double monthlyInterestRate = Convert.ToDouble(inputStringAnnualInterestRate) / 1200;
        Console.WriteLine(monthlyInterestRate);

        Console.Write("How long is your loan (in years)?");
        string inputStringLoanInYears = Console.ReadLine();
        double numberOfMonthlyPayments = Convert.ToDouble(inputStringLoanInYears) * 12;
        Console.WriteLine(numberOfMonthlyPayments);


        double estimatedMonthlyPayment = (amountBorrowed * monthlyInterestRate) / (1 - Math.Pow((1 + monthlyInterestRate), -numberOfMonthlyPayments));
        Console.WriteLine(estimatedMonthlyPayment);

        Console.WriteLine("Your estimated payment is {0:C}", estimatedMonthlyPayment);
        Console.WriteLine("You paid {0:C} over the life of the loan", (numberOfMonthlyPayments * estimatedMonthlyPayment));
        Console.WriteLine("Your total interest cost for the loan was {0:C}", ((numberOfMonthlyPayments * estimatedMonthlyPayment) - amountBorrowed));
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
    }
    do
    {
        try
        {
            Console.Write("Do you want to get information for another loan (Y/N)?");
            bContinue = Console.ReadLine();
            /*            if (bContinue == "")
                        {
                            bContinue = String.ToUpper(bContinue);
                        }*/
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
        if (bContinue != "Y" && bContinue != "N")
        {
            Console.WriteLine("Please Enter a Y or an N, capitalized letter");
        }
    } while (bContinue != "Y" && bContinue != "N");
}
