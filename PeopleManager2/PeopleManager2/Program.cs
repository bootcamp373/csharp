﻿using PeopleManager2;
using System;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;
using static PeopleManager2.Person;
using static PeopleManager2.Sorting;


class Program
{
    public static void Main(string[] args)
    {
        List<Person> Persons = new List<Person>();
        Persons.Add(new Person("Cody", 29, "1 Main St", "Detroit", "MI", 11111));
        Persons.Add(new Person("John", 30, "2 Main St", "Detroit", "MI", 22222));
        Persons.Add(new Person("Mike", 31, "3 Main St", "Detroit", "MI", 33333));
        Persons.Add(new Person("Bill", 50, "4 Main St", "Detroit", "MI", 44444));
        Persons.Add(new Person("Will", 33, "5 Main St", "Detroit", "MI", 55555));
        Persons.Add(new Person("Phil", 34, "6 Main St", "Detroit", "MI", 66666));



        Persons.Sort(new AscendingAgeSorter());
 
        foreach (Person Person in Persons)
        {
            Console.WriteLine(Person.Age);
        }


        Persons.Sort(new DescendingZipSorter());
        foreach (Person Person in Persons)
        {
            Console.WriteLine(Person.ZipCode);
        }

        //testing "move" method here
        Console.WriteLine("Person[1]  moves");
        Persons[1].Move("8 Main St.", "Livonia", "MI", 77777);

        Console.WriteLine(Persons[1].City);
        //will change to call display method instead but move is currently working





    }
}