﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager2
{
    public class Person : IComparable<Person>
    {
        public string Name { get; set; }
        public int Age { get; private set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public int ZipCode { get; set; }


        public Person(string Name, int Age, string Address, string City, string State, int ZipCode)
        {
            this.Name = Name;
            this.Age = Age;
            this.Address = Address;
            this.City = City;
            this.State = State;
            this.ZipCode = ZipCode;
        }

        public class DescendingAgeSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return y.Age - x.Age;
            }
        }
        public class AscendingAgeSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return x.Age - y.Age;


            }
        }
        public class DescendingZipSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return y.ZipCode - x.ZipCode;
            }
        }
        public class AscendingZipSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return x.ZipCode - y.ZipCode;


            }
        }

        // methods
        public void HaveABirthday()
        {
            this.Age++;
        }

        public void Move(string Address, string City, string State, int ZipCode)

        {
            this.Address = Address;
            this.City = City;
            this.State = State;
            this.ZipCode = ZipCode;
        }

        public virtual void Display()
        {
            Console.WriteLine("Person name: {0} , is {1} years old and resides at :", this.Name, this.Age);
            Console.WriteLine("Address : {0} City: {1} State: {2} ZipCode: {3}", this.Address, this.City, this.State, this.ZipCode);
        }

        // interface method 
        public int CompareTo(Person other)
        {
            return Name.CompareTo(other.Name);
        }


    }
}
