﻿// See https://aka.ms/new-console-template for more information
using System;

namespace Celsius
{
    class FahrenheitToCelsius
    {
        static void Main(string[] args)
        {
            Console.Write("Enter temperature in Fahrenheit : ");
            double Fahrenheit = Convert.ToDouble(Console.ReadLine());
            double Celsius = (Fahrenheit - 32) * 5 / 9;
            //Console.WriteLine("The converted celsius temperature is : " + Celsius);
            Console.WriteLine("The converted celsius temperature is : " + Math.Round(Celsius, 1));

            Console.ReadLine();
        }
    }
}
