﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace AnotherCustomStore
{
    public class InventoryItem
    {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string Description { get; set; }
            public decimal Price { get; set; }
            public int Quantity { get; set; }

            public InventoryItem(string id, string name, string type, string description, decimal price, int quantity)
            {
                Id = id;
                Name = name;
                Type = type;
                Description = description;
                Price = price;
                Quantity = quantity;
            }

        //public class ToppingItem : InventoryItem
        //{
        //    public string Bacon { get; set; }
        

        //    public ToppingItem(string id, string name, string type, string description, decimal price, int quantity, string Bacon) : base(id, name, type, description, price, quantity)
        //    {
        //        Bacon = Bacon;
               
        //    }
        //}



        public void DisplayDetails()
        {
            Console.WriteLine("Pizza ID" + "  " + Id);
            Console.WriteLine("Pizza Name" + "  " + Name);
            Console.WriteLine("Pizza Type" + " " + Type);
            Console.WriteLine("Pizza Description" + " " + Description);
            Console.WriteLine("Pizza Price" + " " + Price);
            Console.WriteLine("Pizza Quantity" + " " + Quantity);

        }

        public override string ToString()
        {
            return 
            "Pizza ID" + "  " + Id + "Pizza Name" + "  " + Name + "Pizza Type" + " " + Type + "Pizza Description" + " " 
            + Description  + "Pizza Price" + " " + Price 
            + "Pizza Quantity" + " " + Quantity;

        }


    }


}
