﻿using AnotherCustomStore;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq.Expressions;

namespace ConsoleApp1
{


    public class Program
    {
        public static List<InventoryItem> items = null;
        public static StreamWriter logfile = null;


        static void Main(string[] args)
        {


            DateTime runTime = DateTime.Now;
            StreamWriter logfile = null;
            StreamReader inputInventoryFile = null;

            GetInventory(ref logfile);

            while (true)
            {
                foreach (InventoryItem item in items)
                {
                    Console.WriteLine("{0} {1} {2} ", item.Id, item.Name, item.Type);
                }
                Console.WriteLine("   ");

                int pizzaSelection = 0;
                Console.WriteLine("Choose a Pizza for more details (enter a number 1-12)");
                pizzaSelection = Convert.ToInt32(Console.ReadLine());
                DisplayDetails(items, pizzaSelection);
            }





            static void GetInventory(ref StreamWriter logfile)
            {
                DateTime runTime = DateTime.Now;
                string logFileName = @"C:\Academy\AddressLabel\AnotherCustomStore\AnotherCustomStore\Pizza.log";
                // open the log file 
                string inventoryFile = @"C:\Academy\AddressLabel\AnotherCustomStore\AnotherCustomStore\PizzaData.txt";



                try
                {
                    using (logfile = new StreamWriter(logFileName, true))
                    {
                        logfile.WriteLine("Loading Inventory file: {0} on {1} at {2}", inventoryFile, runTime.ToString("d", CultureInfo.CreateSpecificCulture("en-US")), runTime.ToString("t", CultureInfo.CreateSpecificCulture("en-us")));
                        // open the inventory file and load inventory list 
                        try
                        {
                            items = ReadInventoryItemsFromFile(inventoryFile);


                            foreach (InventoryItem item in items)
                            {
                            }
                        }

                        catch (Exception ex)
                        {
                            logfile.WriteLine("Error in loading the inventory file: {0}", ex.Message);
                        }
                    }


                }
                catch (Exception ex2)
                {
                    logfile.WriteLine(ex2.ToString());
                }
            }

            static List<InventoryItem> ReadInventoryItemsFromFile(string filePath)
            {
                List<InventoryItem> items = new List<InventoryItem>();

                string[] lines = File.ReadAllLines(filePath);
                int lineCounter = 0;
                foreach (string line in lines)
                {
                    lineCounter++;
                    string[] fields = line.Split('|');
                    try
                    {

                        InventoryItem InventoryItem = new InventoryItem(fields[0], fields[1], fields[2], fields[3], decimal.Parse(fields[4]), int.Parse(fields[5]));
                        items.Add(InventoryItem);



                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error reading line {0} with exception : {1}", lineCounter, ex.Message);
                    }

                }

                return items;


            }//end
            static void DisplayDetails(List<InventoryItem> InventoryItems, int Pizzaselection)
            {
                string outputFile = @"C:\Academy\AddressLabel\AnotherCustomStore\AnotherCustomStore\PizzaReceipt.txt";

                InventoryItem inventoryItem = (
                        from item in InventoryItems
                        where item.Id == Convert.ToString(Pizzaselection)
                        select item).FirstOrDefault();
                inventoryItem.DisplayDetails();
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1 - Purchase Item");
                Console.WriteLine("2 - Return to Main Menu");
                //Console.WriteLine("3 - Add bacon to pizza?");

                string menuInput = Console.ReadLine();
                switch (menuInput)
                {
                    case "1":
                        if (inventoryItem != null)
                        {
                            try
                            {
                                using (StreamWriter outputfile = new StreamWriter(outputFile, true))
                                {
                                    outputfile.WriteLine("--Pizza Receipt--");
                                    Console.WriteLine( inventoryItem.ToString());

                                    outputfile.WriteLine(inventoryItem.ToString());

                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"Error opening file: {ex.Message}");


                            }




                        }
                        break;
                    case "2":
                        return;
                        break;
                    default:
                        Console.WriteLine("Invalid input");
                        break;
                }
            }
        }
    }
}
