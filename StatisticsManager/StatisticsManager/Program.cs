﻿using System.Runtime.Intrinsics.X86;

double[] arr = new double[8] { 10, 17, 25, 30, 40, 55, 90, 70 };
string command;


do
{
    Console.WriteLine("Commands ---- > Stats  -  Quit");
    Console.Write("What would you like to do? ");
    command = Console.ReadLine();


    if (command == "Stats")
    {
        Console.Write("GetAverage | GetMedian | GetLargest | GetSmallest | ");
        string GetItem = Console.ReadLine();

        if (GetItem == "GetAverage")
        {
            int? Average = AvgSearch(arr);
        }

        if (GetItem == "GetSmallest")
        {
            int? Smallest = MinSearch(arr);
        }

        if (GetItem == "GetLargest")
        {
            int? Largest = MaxSearch(arr);
        }

        if (GetItem == "GetMedian")
        {
            int? Median = MedSearch(arr);
        }

        if (arr is null)
        {
            Console.WriteLine("Stats doesn't work, try again");
        }
        else
        {
            //Console.WriteLine("Average = " + Average);

        }

    }
    else if (command != "Quit")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
} while (command != "Quit");


static int? AvgSearch(double[] arr)
{
    try
    {
        double avg = Queryable.Average(arr.AsQueryable());
        Console.WriteLine("Average = " + avg);
        return (int?)avg;
    }
    catch (Exception x)
    {
        return null;
    }
}

static int? MinSearch(double[] arr)
{
    try
    {
        double min = Queryable.Min(arr.AsQueryable());
        Console.WriteLine("Smallest Number = " + min);
        return (int?)min;
    }
    catch (Exception x)
    {
        return null;
    }
}

static int? MaxSearch(double[] arr)
{
    try
    {
        double max = Queryable.Max(arr.AsQueryable());
        Console.WriteLine("Largest Number = " + max);
        return (int?)max;
    }
    catch (Exception x)
    {
        return null;
    }
}

static int? MedSearch(double[] arr)
{
    try
    {
        Array.Sort(arr);
        if (arr.Length % 2 != 0)
        {
            int middleIndex = (int)Math.Floor((Decimal)arr.Length / 2);
            Console.WriteLine("Median" + arr[middleIndex]);
            return (int?)middleIndex;


        }
        else
        {
            return null;
        }

    }
    catch (Exception x)
    {
        return null;
    }
}