﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStore
{
    public sealed class Giraffe : Animal, IHerbivore
    {
        public override string Name { get; set; }
        public override int Age { get; set; }

        public string Graze()
        {
            return "The giraffe stretches its long neck to reach leaves on tall trees.";
        }


        public override string MakeSound()
        {
            return "Hum";
        }
    }
}
