﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStore
{
    public abstract class Animal
    {
        public abstract string Name { get; set; }
        public abstract int Age { get; set; }




        //methods
        public abstract string MakeSound();

        public void DisplayInfo()
        {
            Console.WriteLine("Name : {0}  Age: {1}", this.Name, this.Age);
        }

        public void  Hunt()
        {
            Console.WriteLine("The lion stalks its prey and pounces to catch it");
        }

        public void Graze()
        {
            Console.WriteLine("The lion stalks its prey and pounces to catch it");
        }
    }
}
