﻿using CustomStore;
using static CustomStore.Animal;

namespace CustomStore
{
    public class Program
    {

        static void Main(string[] args)
        {

            //array init
            Animal[] animals =
            {
                new Giraffe { Name = "Cody", Age = 29 },
                new Giraffe { Name = "Mike" , Age = 30},
                new Elephant { Name = "Ellie", Age = 29},
                new Elephant { Name = "Al", Age = 45},
                new Lion { Name = "Roary" , Age = 58},
                new Lion { Name = "Simba" , Age = 12}
            };

            //these are for the hunt vs graze logic
            IHerbivore Herbivore = null;
            ICarnivore Carnivore = null;
            foreach (Animal animal in animals)
            {
                Console.WriteLine("This animal's name is : {0} . It is : {1} years old. It makes the following sound: {2}", animal.Name, animal.Age, animal.MakeSound());

                if (animal is IHerbivore)
                {
                    Console.WriteLine(((IHerbivore)animal).Graze());
                }
                else if (animal is ICarnivore)
                {
                    Console.WriteLine(((ICarnivore)animal).Hunt());
                }
            }

        }
    }
}
