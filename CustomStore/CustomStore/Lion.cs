﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStore
{
    public class Lion : Animal, ICarnivore
    {
        public override string Name { get; set; }
        public override int Age { get; set; }

        public string Hunt()
        {
            return "The lion hunts and eats gazelles.";
        }

        public override string MakeSound()
        {
            return "Roar";
        }
    }
}
