﻿using ClassPay;
using System;
using System.ComponentModel;

class Program
{
    public int myVar;  //a non-static field
    public static void Main()
    {
        Class1 p1 = new Class1();  //a object of class
        p1.firstName = "Cody";
        //p1.lastName = "Allison";
        //p1.employeeID = 1;
        //p1.yearJoined = 2016;
        //p1.jobtitle = "Software Engineer";
        //p1.department = "IT";
        //p1.salary = 100000000;


        Console.WriteLine(p1.firstName);
        //Console.WriteLine(p1.lastName);
        //Console.WriteLine(p1.employeeID);
        //Console.WriteLine(p1.yearJoined);
        //Console.WriteLine(p1.jobtitle);
        //Console.WriteLine(p1.department);
        //Console.WriteLine(p1.salary);


        Console.ReadKey();
    }
}