﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFBlockbusters.Models;
using Microsoft.EntityFrameworkCore;


namespace EFBlockbusters
{
    public class BlockbusterMovieEntities : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Movie> Movie { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Movies;Integrated Security=true;");
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Category>()
        //        .HasMany(Category => Category.Movies)
        //        .WithOne(Movie => Movie.Category)
        //        .HasForeignKey(Movie => Movie.MovieID);
        //}

        protected override void OnModelCreating(
       ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
            .HasMany(category => category.Movies)
            .WithOne(movie => movie.Category);

            modelBuilder.Entity<Category>().HasData(
            new Category
            {
                CategoryID = 1,
                CategoryName = "Kids"
            },
            new Category
            {
                CategoryID = 2,
                CategoryName = "Action"
            },
            new Category
            {
                CategoryID = 3,
                CategoryName = "Western"
            },
            new Category
            {
                CategoryID = 4,
                CategoryName = "Sci-Fi"
            },
            new Category
            {
                CategoryID = 5,
                CategoryName = "Comedy"
            },
             new Category
             {
                 CategoryID = 6,
                 CategoryName = "Superhero"
             }
            );
            modelBuilder.Entity<Movie>().HasData(
             new Movie
             {
                 Title = "Finding Nemo",
                 MovieID = 1,
                 Description = "Fish movie (animated)",
                 CategoryID = 1
             },
             new Movie
             {
                 Title = "Mission Impossible 12",
                 MovieID = 2,
                 Description = "Tom Cruise movie with explosions",
                 CategoryID = 2
             },
             new Movie
             {
                 Title = "Walker, Texas Ranger",
                 MovieID = 3,
                 Description = "Cowboy movie",
                 CategoryID = 3

             },
              new Movie
              {
                  Title = "Star Trek",
                  MovieID = 4,
                  Description = "Space movie",
                  CategoryID = 4
              },
              new Movie
              {
                     Title = "Bruce Almighty",
                     MovieID = 5,
                     Description = "Comedy movie with Jim Carrey",
                     CategoryID = 5
               },
               new Movie
               {
                   Title = "Spider-Man",
                   MovieID = 6,
                   Description = "Peter Parker movie",
                   CategoryID = 6
               }




             );
             




        }
    }
}
