﻿using FileProcessing;
using System.Globalization;

namespace ClassPlay
{
    public class Program
    {

        static void Main(string[] args)
        {
            string payrollDirLocation = @"C:\Academy\AddressLabel\FileProcessing2\FileProcessing2\";
            string[] payrollFileNames = { "PayrollData1.txt", "PayrollData2.txt", "PayrollData3.txt" };
            DateTime runTime = DateTime.Now;
            StreamWriter logfile = null;
            StreamReader inputPayrollFile = null;
            string logFileName = @"C:\Academy\AddressLabel\FileProcessing2\FileProcessing2\FileProcessing2.log";
            int numberOfFieldsPer = 0;
            int personCounter = 0;
            try
            {
                using (logfile = new StreamWriter(logFileName, true))
                {
                    foreach (string payrollFileName in payrollFileNames)
                    {
                        logfile.WriteLine("Processing file: {0} on {1} at {2}", payrollDirLocation + payrollFileName[0], runTime.ToString("d", CultureInfo.CreateSpecificCulture("en-US")), runTime.ToString("t", CultureInfo.CreateSpecificCulture("en-us")));
                        try
                        {
                            using (inputPayrollFile = new StreamReader(payrollDirLocation + payrollFileName))
                            {
                                personCounter = 0;
                                while (inputPayrollFile.EndOfStream != true)
                                {
                                    personCounter++;
                                    string text = inputPayrollFile.ReadLine();


                                    string[] inputFields = text.Split("|");
                                    numberOfFieldsPer = inputFields.Length;
                                    if (numberOfFieldsPer != 3)
                                    {
                                        logfile.WriteLine("Payroll File {0}, line {1} , 3 fields required  - found {2} - make sure each record is on a new line", payrollDirLocation + payrollFileNames[0], lineCounter, numberOfFieldsPer);
                                        Console.WriteLine("Payroll File {0}, line {1} , 3 fields required - found {2} - make sure each record is on a new line", payrollDirLocation + payrollFileNames[0], lineCounter, numberOfFieldsPer);
                                        continue;
                                    }
                                    else
                                    {
                                        TimeCard timeCard = TimeCard.CreateTimeCard(text);
                                        Console.WriteLine("Payroll File: {3} Employee: {0} Total Hours Worked: {1} GrossPay: {2}", timeCard.Name, timeCard.HoursWorked, timeCard.GetGrossPay(), payrollFileName);
                                    }
                                }
                            }
                        }
                        catch (FileNotFoundException ex1)
                        {
                            Console.WriteLine("Error:  file not found: " + ex1.Message);
                        }
                        catch (Exception ex2)
                        {
                            Console.WriteLine("Error: file not found : " + ex2.Message);
                        }
                    }
                }
            }
            catch (FileNotFoundException ex1)
            {
                Console.WriteLine("Error: file missing : " + ex1.Message);
            }
            catch (Exception ex2)
            {
                Console.WriteLine("Error: file missing : " + ex2.Message);
            }
            finally
            {
                if (logfile != null)
                {
                    logfile.Close();
                }
                if (inputPayrollFile != null)
                {
                    inputPayrollFile.Close();
                }
            }
        }
    }
}