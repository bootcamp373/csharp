﻿namespace Searcher;

class Program
{
    static void Main(string[] args)
    {
        string command;
        string[] items = { "Sausage Breakfast Taco",
        "Potato and Egg Breakfast Taco", "Sausage and Egg Biscuit",
        "Bacon and Egg Biscuit", "Pancakes"};
        decimal[] prices = { 3.99M, 3.29M, 3.70M, 3.99M, 4.79M };

        do
        {
            Console.WriteLine("Commands ---- > Order Food  -  Quit");
            Console.Write("What would you like to do? ");
            command = Console.ReadLine();


            if (command == "Order Food")
            {
                Console.Write("Select a food item: Sausage Breakfast Taco | Potato and Egg Breakfast Taco | Sausage and Egg Biscuit | Bacon and Egg Biscuit | Pancakes ");
                string orderItem = Console.ReadLine();
                decimal? itemPrice = MenuSearch(items, prices, orderItem);


                if (prices is null)
                {
                    Console.WriteLine("Food Menu Item doesn't exist, please order again");
                }
                else
                {
                    Console.WriteLine("Your order was  {0:s}", orderItem);
                    Console.WriteLine("That order costs  {0:c}", itemPrice);


                }




            }
            else if (command != "Quit")
            {
                Console.WriteLine("**Error: unrecognized command");
            }
        } while (command != "Quit");

    }
    static decimal? MenuSearch(string[] items, decimal[] prices, string orderItem)
    {
        try
        {
            int itemSpot = Array.IndexOf(items, orderItem);
            decimal ItemPrice = prices[itemSpot];
            return ItemPrice;
        }
        catch (Exception x)
        {
            return null;
        }
    }

}
