﻿using BooksAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace BooksAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
 
    public class Book : Controller
    {
        private List<Book> _books = new List<Book>
        {
            new Book
            {
                 BookID = 1,
                Title = "Book1",
                Author  = "Cody",
                Publisher = "Good Books",
                YearPublished = 1993,
                Price = 9.99
            },
            new Book
            {
                BookID = 2,
                Title = "Book2",
                Author  = "Cody",
                Publisher = "Good Books",
                YearPublished = 1993,
                Price = 19.99
            },
        };

        public int BookID { get; private set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public int YearPublished { get; set; }
        public double Price { get; set; }

        [HttpGet]
        public IEnumerable<Book> Get()
        {
            return _books;
        }

        [HttpGet("{BookID}")]
        public Book Get(int BookID)
        {
            return _books.FirstOrDefault(p => p.BookID == BookID);
        }
    }
}
