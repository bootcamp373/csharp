﻿using Book2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.SqlServer.Server;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using SqlConnection = Microsoft.Data.SqlClient.SqlConnection;
using Microsoft.Extensions.Configuration;
using System.Drawing;


namespace Book2.Controllers
{
    public class program
    {
        public static string connStr = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static SqlConnection cn = new SqlConnection(connStr);

        [Route("api/[controller]")]
        [ApiController]



        public class BookController : ControllerBase
        {



            //SqlConnection cn = new SqlConnection(GetConnectionString());

            //public static string GetConnectionString()
            //{
            //    ConfigurationBuilder builder = new ConfigurationBuilder();
            //    builder.SetBasePath(Directory.GetCurrentDirectory())
            //    .AddJsonFile("appsettings.json", optional: false,
            //     reloadOnChange: true);
            //    IConfiguration config = builder.Build();

            //    string? ConStr = config["ConnectionStrings:BooksDb"];
            //    Console.WriteLine(ConStr);

            //    return ConStr;
            //}

                public List<Book> _books = new List<Book>
            {
                new Book
                {
                    BookID = 1,
                    Title = "Book1",
                    Author  = "Cody",
                    Publisher = "Good Books",
                    YearPublished = 1993,
                    Price = 9.99
                },
                new Book
                {
                    BookID = 2,
                    Title = "Book2",
                    Author  = "Cody",
                    Publisher = "Good Books",
                    YearPublished = 1993,
                    Price = 19.99
                },
                  new Book
                {
                    BookID = 2,
                    Title = "Book2",
                    Author  = "Cody",
                    Publisher = "Good Books",
                    YearPublished = 1987,
                    Price = 19.99
                },
                    new Book
                {
                    BookID = 3,
                    Title = "Book3",
                    Author  = "Cody",
                    Publisher = "Good Books",
                    YearPublished = 1908,
                    Price = 8.99
                },
                      new Book
                {
                    BookID = 4,
                    Title = "Book4",
                    Author  = "Cody",
                    Publisher = "Good Books",
                    YearPublished = 2021,
                    Price = 7.99
                },
                        new Book
                {
                    BookID = 5,
                    Title = "Book5",
                    Author  = "Cody",
                    Publisher = "Good Books",
                    YearPublished = 2023,
                    Price = 5.99
                },


        };
           
            
            [HttpGet]
            public IEnumerable<Book> Get()
            {
                return _books;
            }


            [HttpGet("{BookID}")]
            public Book Get(int BookID)
            {
                return _books.FirstOrDefault(p => p.BookID == BookID);
            }

            [HttpPost("{BookID}")]
            public ActionResult Post([FromBody] Book book)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                book.BookID = 6;
                //book.Author = "Cody Allison";
                //book.YearPublished = 1993;
                //book.Publisher = "Good Books";
                //book.Price = 39.99;
                //Book.Add(_books);
                _books.Add(book);



                string location = $"api/Book/{book.BookID}";
                return Created(location, _books);
            }

            [HttpPut("{BookID}")]
            public ActionResult Put(Book book, int BookID)
            {
                Book p = _books.FirstOrDefault(p => p.BookID == BookID);
                Console.WriteLine("Found book" + p.BookID);
                if (p == null)
                {
                    return NotFound();
                }
                else
                {
                    p.Title = book.Title;
                    p.Author = book.Author;
                    p.Publisher = book.Publisher;
                    p.YearPublished = book.YearPublished;
                    p.Price = book.Price;
                    return Ok();
                }
            }

            [HttpDelete("{BookID}")]
            public ActionResult Delete(int BookID)
            {
                Console.WriteLine(BookID);
                Book toBeDeleted = _books.FirstOrDefault(p => p.BookID == BookID);
                // Console.WriteLine("Found pet" + toBeDeleted.Name);

                if (toBeDeleted == null)
                {
                    return NotFound();
                }
                else
                {
                    _books.Remove(toBeDeleted);
                    return Ok();
                }
            }




            //static public void LoadData (SqlConnection cn)
            //{
            //    cn.Open();
            //    string sql = "Select BookID From Book";
            //    SqlCommand cmd = new SqlCommand(sql, cn);
            //    SqlDataReader dr = cmd.ExecuteReader();
            //    while (dr.Read())
            //    {
            //        string Title = (string)dr["Title"];
            //        Console.WriteLine(Title);
            //    }
            //    dr.Close();
            //    cn.Close();
            //}

        }
    }

}


