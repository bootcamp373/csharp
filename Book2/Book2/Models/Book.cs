﻿namespace Book2.Models
{
    public  class Book
    {
        public int BookID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public int YearPublished { get; set; }
        public double Price { get; set; }

        //internal static void Add(Book books)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
