﻿

using Azure.Core;
using Book2.Models;
using Microsoft.Data.SqlClient;

namespace Book2
{
    public class BookAdd
    {
        public static string connStr = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static SqlConnection cn = new SqlConnection(connStr);

        static void Main(string[] args)
        {
            // create an array of books to be added to the database
            Book[] booksToAdd = new Book[] {
            new Book { BookID = 1, Title = "The Prefect", Author = "Alistair Reynolds", Publisher = "Random House", YearPublished = 2013, Price = 15.75 },
            new Book { BookID = 2, Title = "Dream of 1000 nights", Author = "Alistair Reynolds", Publisher = "Tor Publishing", YearPublished = 2011, Price = 13.25 },
            new Book { BookID = 3, Title = "Leviathan Wakes", Author = "James S.A. Corey", Publisher = "Wow Publishing", YearPublished = 2009, Price = 17.59 },
            new Book { BookID = 4, Title = "A Method For Madness", Author = "David Gerrold", Publisher = "Tor Publishing", YearPublished = 2024, Price = 25.33 },
            new Book { BookID = 5, Title = "The Unbearable Lightness of Being", Author = "Milan Kundera", Publisher = "Harper and Row", YearPublished = 1984, Price = 20.15 },
        };
            cn.Open();
            // loop through the array and add each book to the database
            foreach (Book book in booksToAdd)
            {
                string sql = "INSERT INTO Book (BookID, Title, Author, Publisher, YearPublished, Price) VALUES (@BookId, @Title, @Author, @Publisher, @YearPublished, @Price)";

                using (SqlCommand command = new SqlCommand(sql, cn))
                {
                    command.Parameters.AddWithValue("@BookID", book.BookID);
                    command.Parameters.AddWithValue("@Title", book.Title);
                    command.Parameters.AddWithValue("@Author", book.Author);
                    command.Parameters.AddWithValue("@Publisher", book.Publisher);
                    command.Parameters.AddWithValue("@YearPublished", book.YearPublished);
                    command.Parameters.AddWithValue("@Price", book.Price);
                    command.ExecuteNonQuery();
                }
            }
            cn.Close();
        }
    }
}
