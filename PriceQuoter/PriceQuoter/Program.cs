﻿string productCode = "";
int qty = 0;
double perUnitPrice = 0;
double totalPriceOrder;
string command = "";

Console.Write("Enter production code: ");
productCode = Console.ReadLine();
Console.Write("Enter quantity: ");
qty = Convert.ToInt32(Console.ReadLine());
Console.Write("Enter a command: ");
command = Console.ReadLine();



switch (productCode)
{
    case "BG-127":
        {
            if (qty >= 1 && qty <= 24)
            {
                perUnitPrice = 18.99;
            }
            else if (qty >= 25 && qty <= 50)
            {
                perUnitPrice = 17.00;
            }
            else if (qty >= 51)
            {
                perUnitPrice = 14.49;
            }
            break;
        }
    case "WRTR-28":
        {
            if (qty >= 1 && qty <= 24)
            {
                perUnitPrice = 125.00;
            }
            else if (qty >= 25 && qty <= 50)
            {
                perUnitPrice = 113.75;
            }
            else if (qty >= 51)
            {
                perUnitPrice = 99.99;
            }

            break;
        }
    case "GUAC-8":
        {
            if (qty >= 1 && qty <= 24)
            {
                perUnitPrice = 8.99;
            }
            else if (qty >= 25 && qty <= 50)
            {
                perUnitPrice = 8.99;
            }
            else if (qty >= 51)
            {
                perUnitPrice = 7.49;
            }

            break;
        }
    default:
        {
            Console.WriteLine($"{productCode} product not found");
            break;
        }
}


totalPriceOrder = (double)qty * perUnitPrice;

do
{
    Console.WriteLine("Commands -->");
    Console.WriteLine(" ORDER : Place an order");
    Console.WriteLine(" RETURN : Return an order");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();
    if (command == "ORDER")
    {
        Console.WriteLine($"{productCode} {qty} {perUnitPrice:C}  Total price of order {totalPriceOrder:C}");
        // do something (example displays "ordering…")
    }
    else if (command == "RETURN")
    {
        Console.WriteLine($"{productCode} {qty} {perUnitPrice:C}  Return order {totalPriceOrder:C}");

        // do something (example displays "returning…")
    }
    else if (command != "QUIT")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
} while (command != "QUIT");

//if (totalPriceOrder > 0)
//{
//    Console.WriteLine($"{productCode} {qty} {perUnitPrice:C}  Total price of order {totalPriceOrder:C}");
//}
