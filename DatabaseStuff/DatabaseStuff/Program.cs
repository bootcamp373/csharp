﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DatabaseStuff
{
    public class testdatabasestring
    {


        static void Main(string[] args)
        {
            SqlConnection cn = new SqlConnection(GetConnectionString());
            try
            {

                //Console.WriteLine(GetConnectionString);
                //DisplayCategoryCount(cn);
                //DisplayTotalInventoryValue(cn);
                //AddAShipper(cn);
                //ChangeShipperName(cn);
                //for (int ShipperId = 5; ShipperId <= 8; ShipperId++)
                //{
                //    DeleteAShipper(cn, ShipperId);
                //}




                //DisplayAllProducts(cn);
                //DisplayProductsInCategory(cn, "Beverages");
                //DisplayProductsForSupplier(cn, "Beverages", "Exotic Liquids");
                //DisplaySupplierProductCounts(cn);
                //DisplayTenMostExpensiveProducts(cn);
                DisplaySalesByCategory(cn, "Beverages", "1998");






            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                //Console.WriteLine("Finally");
                cn.Close();
            }




            static string GetConnectionString()
            {

                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false,
                 reloadOnChange: true);
                IConfiguration config = builder.Build();

                string? ConStr = config["ConnectionStrings:Northwind"];
                Console.WriteLine(ConStr);

                return ConStr;
            }




            static void DisplayCategoryCount(SqlConnection cn)
            {

                cn.Open();
                string sql = "Select Count(*) From Categories";
                SqlCommand cmd = new SqlCommand(sql, cn);
                int productCount = (int)cmd.ExecuteScalar();

                Console.WriteLine($"We carry {productCount} Categories");
                //return Convert.ToString(productCount);
                cn.Close();

            }

            static void DisplayTotalInventoryValue(SqlConnection cn)
            {
                cn.Open();

                //int  stockCount = 0;
                string sql = "Select sum(UnitsInStock * UnitPrice) From Products";
                SqlCommand cmd = new SqlCommand(sql, cn);
                decimal InventoryValue = (decimal)cmd.ExecuteScalar();

                Console.WriteLine("Total {0}", InventoryValue);

                Console.WriteLine("Total Value = ${0:f2}", InventoryValue);
                cn.Close();

            }

             static void AddAShipper(SqlConnection cn)
            {
                cn.Open();
                string sql = "INSERT INTO Shippers  VALUES('My Shipping','555-555-5555')";
                SqlCommand cmd = new SqlCommand(sql, cn);
                int rowsChanged = cmd.ExecuteNonQuery();
                cn.Close();
                Console.WriteLine($"{rowsChanged} Shippers added");
            }


            static void DeleteAShipper(SqlConnection cn, int ShipperID)
            {
                cn.Open();
                string sql = $"Delete from Shippers WHERE ShipperID = " + Convert.ToString(ShipperID);
                SqlCommand cmd = new SqlCommand(sql, cn);
                int rowsDeleted = cmd.ExecuteNonQuery();
                cn.Close();
                Console.WriteLine($"{rowsDeleted} Shippers deleted with ShipperID = {ShipperID}");
            }

             static void ChangeShipperName(SqlConnection cn)
            {
                cn.Open();
                string sql = "Update Shippers Set CompanyName = 'Our Shipping' WHERE ShipperID = 3";
                SqlCommand cmd = new SqlCommand(sql, cn);
                int rowsChanged = cmd.ExecuteNonQuery();
                cn.Close();
                Console.WriteLine($"{rowsChanged} Shippers changed");
            }


            static void DisplayAllProducts(SqlConnection cn)
            {
                cn.Open();
                string sql =
                "Select ProductID, ProductName, UnitsInStock, UnitPrice " +
                "From Products";
                SqlCommand cmd = new SqlCommand(sql, cn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int productID = (int)dr["ProductID"];
                    string productName = (string)dr["ProductName"];
                    Int16 UnitsInStock = (Int16)dr["UnitsInStock"];

                    decimal unitPrice = (decimal)dr["UnitPrice"];
                    Console.WriteLine(
                    $"[{productID}] {productName} {UnitsInStock} - {unitPrice:C}");
                }
                dr.Close();
                cn.Close();

            }

            static void DisplayProductsInCategory(SqlConnection cn, string categoryName)
            {
                cn.Open();
                string sql = "Select ProductID, ProductName, UnitsInStock, UnitPrice From Products"
                + " Inner join Categories On Products.CategoryID = Categories.CategoryID"
                + " Where  "
                //+ categoryName + "'";
                + "categoryName = @categoryName";
                SqlParameter param = new SqlParameter("categoryName", categoryName);
                param.Value = categoryName;
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    cmd.Parameters.Add(param);


                    using SqlDataReader dr = cmd.ExecuteReader();
                    {
                        while (dr.Read())
                        {
                            int productID = (int)dr["ProductID"];
                            string productName = (string)dr["ProductName"];
                            Int16 UnitsInStock = (Int16)dr["UnitsInStock"];

                            decimal unitPrice = (decimal)dr["UnitPrice"];
                            Console.WriteLine(
                            $"[{productID}] {productName} {UnitsInStock} - {unitPrice:C}");
                        }
                        dr.Close();
                        cn.Close();
                    }
                }


            }

             static void DisplayProductsForSupplier(SqlConnection cn, string categoryName, string supplierName)
            {
                cn.Open();
                string sql = "Select p.ProductID,   p.ProductName,   p.UnitsInStock,   p.UnitPrice From Products p "
                     + "Inner join Categories c On p.CategoryID = c.CategoryID Inner Join Suppliers s On p.SupplierID = s.SupplierID Where "
                     /*+ "c.CategoryName = '"
                     + categoryName + "' AND s.CompanyName = '" + supplierName + "'"*/
                     + "categoryName = @categoryName AND s.CompanyName = @supplierName"
                     ;



                SqlParameter param = new SqlParameter("categoryName", categoryName);
                param.Value = categoryName;
                SqlParameter param2 = new SqlParameter("supplierName", supplierName);
                param2.Value = supplierName;
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    cmd.Parameters.Add(param);
                    cmd.Parameters.Add(param2);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            int productID = (int)dr["ProductID"];
                            string productName = (string)dr["ProductName"];
                            Int16 unitsInStock = (Int16)dr["UnitsInStock"];
                            decimal unitPrice = (decimal)dr["UnitPrice"];
                            Console.WriteLine(
                            $"{supplierName}  +  {categoryName} :  [{productID}] {productName} - {unitsInStock} - {unitPrice:C}");
                        }
                        dr.Close();
                        cn.Close();
                    }
                }
            }

             static void DisplaySupplierProductCounts(SqlConnection cn)
            {
                cn.Open();

                string sql = "SELECT Suppliers.CompanyName, COUNT(Products.ProductID) AS NumberOfProducts FROM Products "
                            + "RIGHT JOIN Suppliers ON Products.SupplierID = Suppliers.SupplierID "
                            + "GROUP BY Suppliers.CompanyName "
                            + "ORDER BY COUNT(Products.ProductID) DESC";


                SqlCommand cmd = new SqlCommand(sql, cn);

                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    string supplierName = (string)dr["CompanyName"];
                    int ItemCount = (int)dr["NumberOfProducts"];
                    Console.WriteLine($"{supplierName}  +  {ItemCount}");
                }
                dr.Close();
                cn.Close();
            }

             static void DisplayCustomersAndSuppliersByCity(SqlConnection cn)
            {
                cn.Open();
                string sql = "SELECT * FROM [Customer and Suppliers by City];";
                SqlCommand cmd = new SqlCommand(sql, cn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    string name = (string)dr["CompanyName"];
                    string cityName = (string)dr["City"];
                    Console.WriteLine($"{cityName} --- {name}");
                }
                dr.Close();
                cn.Close();
            }

            static void DisplayTenMostExpensiveProducts(SqlConnection cn)
            {
                cn.Open();
                SqlCommand cmd =  new SqlCommand("Ten Most Expensive Products", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string productName =
                    (string)dr["TenMostExpensiveProducts"];
                    decimal unitPrice = (decimal)dr["UnitPrice"];
                    Console.WriteLine($"{productName} -- {unitPrice:C}");
                }
                dr.Close();
                cn.Close();
            }

            static void DisplaySalesByCategory(SqlConnection cn, string categoryName, string year)
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("DisplaySalesByCategory", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue(categoryName, year);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string productName = (string)dr["ProductName"];
                    int total = (int)dr["Total"];
                    Console.WriteLine($"{productName} -- {total}");
                }
                dr.Close();
                cn.Close();
            }



        }
    }
}
