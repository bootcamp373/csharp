﻿// See https://aka.ms/new-console-template for more information
Console.Write("Enter a renewal date: ");
string str = Console.ReadLine();
DateTime dueDate = Convert.ToDateTime(str);
DateTime GracePeriodDate = dueDate.AddDays(10);
DateTime PolicyCancelDate = dueDate.AddMonths(1);



Console.WriteLine($"GracePeriodDate: {GracePeriodDate}");
Console.WriteLine($"Policy Cancel Date w/ no payment: {PolicyCancelDate}");

