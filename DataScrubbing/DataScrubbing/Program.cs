﻿﻿namespace DataScrubbing;

class Program
{
    static void Main(string[] args)
    {
        string command;
        do
        {
            Console.WriteLine("Scrub  -  Scrub2  -  Scrub3  - NameParse Quit");
            Console.Write("What would you like to do? ");
            command = Console.ReadLine();
            string phoneNumber;
            char charToRemove;
            char charToReplaceWith;

            
            if (command == "Scrub")
            {

                Console.Write("Please enter a phone number: ");
                try
                {
                    phoneNumber = (Console.ReadLine());
                }
                catch (Exception x)
                {

                    Console.WriteLine($"Please enter a valid phone number: {x.Message}");
                    continue;
                }
                Console.WriteLine($"phoneNumber BEFORE = |{phoneNumber}|", phoneNumber);
                ScrubPhone(ref phoneNumber);
                Console.WriteLine($"phoneNumber AFTER  = |{phoneNumber}|", phoneNumber);
                continue;
            }
            if (command == "Scrub2")
            {

                Console.Write("Please enter a phone number: ");
                try
                {
                    phoneNumber = (Console.ReadLine());
                }
                catch (Exception x)
                {

                    Console.WriteLine($"Please enter a valid phone number: {x.Message}");
                    continue;
                }
                Console.Write("Please enter a character to remove from phone number: ");
                try
                {
                    charToRemove = Console.ReadKey().KeyChar; 
                }
                catch (Exception x)
                {

                    Console.WriteLine($"Please enter a valid character to remove: {x.Message}");
                    continue;
                }
                Console.WriteLine($"phoneNumber BEFORE = |{phoneNumber}|", phoneNumber);
                ScrubPhone(ref phoneNumber, charToRemove);
                Console.WriteLine($"phoneNumber AFTER  = |{phoneNumber}|", phoneNumber);
                continue;
            }
            if (command == "Scrub3")
            {

                Console.Write("Please enter a phone number: ");
                try
                {
                    phoneNumber = (Console.ReadLine());
                }
                catch (Exception x)
                {

                    Console.WriteLine($"Please enter a valid phone number: {x.Message}");
                    continue;
                }

                Console.Write("Please enter a character to remove from phone number: ");
                try
                {
                    charToRemove = Console.ReadKey().KeyChar;
                }
                catch (Exception x)
                {

                    Console.WriteLine($"Please enter a valid character to remove: {x.Message}");
                    continue;
                }

                Console.Write("Please enter a character to replace the previous character with: ");
                try
                {
                    charToReplaceWith = Console.ReadKey().KeyChar;
                }
                catch (Exception x)
                {

                    Console.WriteLine($"Please enter a valid character to remove: {x.Message}");
                    continue;
                }
                Console.WriteLine($"phoneNumber BEFORE = |{phoneNumber}|", phoneNumber);
                ScrubPhone(ref phoneNumber, charToRemove, charToReplaceWith);
                Console.WriteLine($"phoneNumber AFTER  = |{phoneNumber}|", phoneNumber);
                continue;
            }
            else if (command != "Quit")
            {
                Console.WriteLine("**Error: unrecognized command");
            }
        } while (command != "Quit");

    }
  
    public static void ScrubPhone(ref string phoneNumber)
    {
        phoneNumber = phoneNumber.Trim();
    }

    public static void ScrubPhone(ref string phoneNumber,
    char charToRemove)
    {
        Console.WriteLine($"ScrubPhone2 charToRemove = |{charToRemove}|", charToRemove);
        phoneNumber = phoneNumber.Replace(charToRemove.ToString(),String.Empty);
    }
    public static void ScrubPhone(ref string phoneNumber,
    char charToRemove, char charToReplaceWith)
    {
        phoneNumber = phoneNumber.Replace(charToRemove, charToReplaceWith);
    }
}

