﻿using System;
using System.IO;
using System.Xml.Linq;

string fileName = @"C:\Academy\AddressLabel\PayrollFileProcessor\PayrollFileProcessor\Payroll1.txt";
string logFileName = @"C:\Academy\AddressLabel\PayrollFileProcessor\PayrollFileProcessor\PayrollFileProcessor.log";


string id;
string name;
decimal rate;
decimal hours;
decimal pay;

StreamReader inputFile = null;
try
{
    inputFile = new StreamReader(fileName);
    while (!inputFile.EndOfStream)
    {
        string text = inputFile.ReadLine();

        //Console.WriteLine(text);
        decimal totalpay = 0;

        id = text.Substring(0, 5);
        name = text.Substring(5, 20);
        rate = Convert.ToDecimal(text.Substring(25, 4));
        hours = Convert.ToDecimal(text.Substring(31,4));

        pay = rate * hours;
        totalpay += pay;


        //Console.WriteLine(id);
        //Console.WriteLine(name);
        //Console.WriteLine(rate);
        Console.WriteLine($"Gross pay totals were {totalpay:C}");


        Console.WriteLine($"{fileName} was processed");
        using (StreamWriter outputFile = new StreamWriter(logFileName, true))
        {
            outputFile.WriteLine($"Procesing file: {fileName} on {DateTime.Now:MM-dd-yyyy} at {DateTime.Now:h:mm:ss}");
            outputFile.WriteLine($"Gross pay totals were {totalpay:C}");
        }



    }



}
catch (Exception ex)
{
    Console.WriteLine($"Error opening file: {ex.Message}");
}
finally
{
    if (inputFile != null) inputFile.Close();
}