﻿using LINQGames;
using System.Net.Http.Headers;
using System.Security.Cryptography;

List<Supplier> suppliers = new List<Supplier> {
new Supplier(101, "ACME", "acme.com"),
new Supplier(201, "Spring Valley", "spring-valley.com"),
new Supplier(301, "Kroger", "kroger.com"),

};
List<Product> products = new List<Product> {
new Product(1, "Dark Chocolate Bar", 4.99M, 10, 101),
new Product(2, "8 oz Guacamole", 5.99M, 27, 201),
new Product(3, "Milk Chocolate Bar", 3.99M, 16, 101),
new Product(4, "8 pkg Chicken Tacos", 15.99M, 7, 201),
new Product(5, "Bacon", 8.99M, 10, 301),
new Product(6, "Frozen Pizza", 15.99M, 7, 301),
new Product(7, "Doritos", 3.99M, 7, 301),
new Product(8, "16 oz Salami", 6.99M, 7, 301),
new Product(9, "16 oz Turkey", 5.99M, 27, 301),
new Product(10, "Pop Tarts", 3.99M, 7, 301),
};

var query4 = from p in products
            where p.ProductName != null
            select p.ProductName;
foreach (string Name in query4)
{
    Console.WriteLine(Name);
}

var query = from p in suppliers
             where p.Name != null
            select p.Name;
foreach (string Name in query)
{
    Console.WriteLine(Name);
}

var priceQuery = from p in products
                 where p.QuantityOnHand >= 10
                 orderby p.QuantityOnHand descending
                 select p.QuantityOnHand;
foreach (int QuantityOnHand in priceQuery)
{
    Console.WriteLine(QuantityOnHand);
 
}

var productQuery = from p in products
                 where p.Price <= 5.00M
                 orderby p.Price ascending
                 select p.Price;
foreach (decimal Price in productQuery)
{
    Console.WriteLine(Price);

}


Console.WriteLine("Supplier 201 Detail");
Supplier Supplier201 = (from s in suppliers
                        where s.SupplierId == 201
                        select s).FirstOrDefault();
Console.WriteLine("ID:" + Supplier201.SupplierId + "Name:" + Supplier201.Name + "URL:" + Supplier201.URL);

Console.WriteLine("    ");
Console.WriteLine("FIrst product over $5");
Product Over5 = (from p in products
                 where p.Price > 5.00M
                 select p).FirstOrDefault();
Console.WriteLine(Over5.ProductName);

Console.WriteLine("    ");
Console.WriteLine("product count under $5");


int under5 = (from p in products
              where p.Price < 5.00M
              select p).Count();
Console.WriteLine(under5);


//get items to be reordered
//foreach(var item in GetItemsToBeReordered(products))
//{
//    Console.WriteLine($"{item.ProductName} has only {item.QuantityOnHand} left");

//}

