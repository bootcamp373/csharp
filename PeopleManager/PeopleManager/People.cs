﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class Person : IComparable<Person>
    {
        public string Name { get; set; }
        public int Age { get; private set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public int ZipCode { get; set; }


        public Person(string name, int age, string address, string city, string state, int zipCode)
        {
            Name = name;
            Age = age;
            Address = address;
            City = city;
            State = state;
            ZipCode = zipCode;
        }
        public class DescendingAgeSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return y.Age - x.Age;
            }
        }
        public class AscendingAgeSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return x.Age - y.Age;
            }
        }

        public int CompareTo(Person? other)
        {
            throw new NotImplementedException();
        }
    }

}
