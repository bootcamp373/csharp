﻿using PeopleManager;
using static PeopleManager.Person;

class Program
{
    public static void Main(string[] args)
    {
        List<Person> People = new List<Person>();
        People.Add(new Person("Cody", 29, "1 Main St", "Detroit", "MI", 11111));
        People.Add(new Person("John", 30, "2 Main St", "Detroit", "MI", 22222));
        People.Add(new Person("Mike", 31, "3 Main St", "Detroit", "MI", 333333));
        People.Add(new Person("Bill", 32, "4 Main St", "Detroit", "MI", 44444));
        People.Add(new Person("Will", 33, "5 Main St", "Detroit", "MI", 55555));
        People.Add(new Person("Phil", 34, "6 Main St", "Detroit", "MI", 66666));
        
        People.Sort(new AscendingAgeSorter());
        foreach (Person x in People)
        {
            Console.WriteLine(x);
        }
    }
}