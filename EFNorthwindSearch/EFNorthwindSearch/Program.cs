﻿using EFNorthwindSearch.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Polly;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Reflection.Metadata;


//static void Main(string[] args)
//{
//    DisplayByCategory();

//DisplayByCategory();
//DisplayCategoryCount();
//DisplayTotalInventoryValue();
//AddAShipper();
DisplayShippers();
ChangeShipperName();
//DeleteShipper();

    static void DisplayByCategory()
    {
        List<string> categoryNames = new List<string>();

        string categoryChoice = "";
        var context = new NorthwindContext();
        var prodAndCategoryQuery =
        from c in context.Categories
        join p in context.Products
        on c.CategoryId equals p.CategoryId
        orderby c.CategoryName
        select new
        {
            p.ProductId,
            p.UnitPrice,
            Category = c.CategoryName,
            p.ProductName
        };
        Console.WriteLine("Product categories are:");
        foreach (var category in prodAndCategoryQuery)
        {
            if (!categoryNames.Contains(category.Category))
            {
                categoryNames.Add(category.Category);
                Console.WriteLine(category.Category);
            }
        }
        Console.WriteLine("Enter a choice:");
        categoryChoice = Console.ReadLine();



        Console.WriteLine($"ID   {"ProductName",-30} {"Price"}");
        Console.WriteLine($"--   {"-----------",-30} {"-----"}");
        foreach (var x in prodAndCategoryQuery)
        {
            if (x.Category == categoryChoice)
            {
                Console.WriteLine($"{x.ProductId,-4} {x.ProductName,-30} {x.UnitPrice:C}");
            }
        }
}
static void DisplayCategoryCount()
{
    var context = new NorthwindContext();
    var countVal = (from a in context.Categories
                select a).Count();
    Console.WriteLine("{0, -15} Count of Categories", countVal);


    //    var prodInCatGroupQuery =
    //from c in context.Categories
    //join p in context.Products
    //on c.CategoryId equals p.CategoryId
    //into catProdGroup
    //select new
    //{
    //    Category = c.CategoryName,
    //    CountOfItems = catProdGroup.Count()
    //};
    //    Console.WriteLine("{0, -15} Count of Items", "Category");
    //    foreach (var pic in prodInCatGroupQuery)
    //    {
    //        Console.WriteLine(
    //        $"{pic.Category,-15} {pic.CountOfItems}");
    //    }

}

static void DisplayTotalInventoryValue()
{
    //string sql = "Select sum(UnitsInStock * UnitPrice) From Products";

    var context = new NorthwindContext();
    // var total=db.tblCartItems.Where(t=>t.CartId == cartId).Sum(i=>i.Price);


    var TotalInvVal = context.Products.Sum(i => i.UnitPrice * i.UnitsInStock);


    //Console.WriteLine("Total {0}", TotalInvVal);
    Console.WriteLine("Total Value = ${0:f2}", TotalInvVal);

}
//static void DisplayTenMostExpensiveProducts()
//{
//    var context = new NorthwindContext();

//    var mostExpensive = NorthwindContext.TenMostExpensiveProductsResult.FromSqlInterpolated(
//    $"[Ten Most Expensive Products]").ToList();
//    foreach (var p in mostExpensive)
//    {
//        Console.WriteLine(
//        $"{p.TenMostExpensiveProducts} -- {p.UnitPrice:C}");
//    }
//}

 static void AddAShipper()
{

    using (var context = new NorthwindContext())
    {
 
        Shipper shipper = new Shipper();
        shipper.Phone = "555-555-5555";
        shipper.CompanyName = "New Shipper";
        context.Shippers.Add(shipper);
        context.SaveChanges();

    }
}

 static void DisplayShippers()
{
    using (var context = new NorthwindContext())
    {
        var getShippers = from s in context.Shippers
                               select s;
        foreach (Shipper s in getShippers)
        {
            Console.WriteLine($"#{s.ShipperId} {s.CompanyName} Ph: {s.Phone}");
        }

    }
}

 static void ChangeShipperName()
{

    using (var context = new NorthwindContext())
    {
        var shippers = (from s in context.Shippers where s.CompanyName == "New Shipper" select s).ToList();
        foreach (var s in shippers)
        {
            s.CompanyName = "Changed Shipper";
        }
        context.SaveChanges();
    }
}

 static void DeleteShipper()
{
    using (var context = new NorthwindContext())
    {
        var shippers = (from s in context.Shippers where s.CompanyName == "Changed Shipper" select s).ToList();
        foreach (var s in shippers)
        {
            context.Shippers.Remove(s);
        }
        context.SaveChanges();
    }
}
