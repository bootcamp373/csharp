﻿
 DirectoryInfo di = new DirectoryInfo(@"C:\Academy\AddressLabel\FileTest\FileTest");
// text files
foreach (var fi in di.GetFiles("*.cs"))
{
    Console.WriteLine(fi.Name);

}

var x = DateTime.Now.ToString("yyyyMMddHHmmssfff");



string currentFile = @"C:\Academy\AddressLabel\FileTest\FileTest\test.txt";
string newFile = @"C:\Academy\AddressLabel\FileTest\FileTest\test-backup" + x + ".txt";

// Make sure the source file does exist
if (!File.Exists(currentFile))
{
    Console.WriteLine($"Error: {currentFile} doesn't exist!");
    return;
}
// Make sure the new file doesn't exist
if (File.Exists(newFile))
{
    Console.WriteLine($"Error: {newFile} already exist!");
    return;
}
// Copy the file. Fail if the new file name already exists
File.Copy(currentFile, newFile);