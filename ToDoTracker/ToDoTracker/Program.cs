﻿using System;
using static System.Net.Mime.MediaTypeNames;

namespace ToDoTracker
{
    public class Program
    {
        static void Main(string[] args)
        {
            string commandnum = "";

            while (commandnum != "5")
            {
                Console.WriteLine("1: Add a task");
                Console.WriteLine("2: Remove a task");
                Console.WriteLine("3: List tasks by due date");
                Console.WriteLine("4: List tasks by difficulty");
                Console.WriteLine("5: Quit the application");

                Console.Write("Your Choice: ");
                commandnum = Console.ReadLine();
                switch (commandnum)
                {
                    case "1":
                        AddATask();
                        break;
                    case "2":
                        RemoveATask();
                        break;
                    case "3":
                        ListTasksByDueDate();
                        break;
                    case "4":
                        ListTasksByDifficulty();
                        break;
                    case "5":
                        break;

                    default: break;
                }
            }
        }
       
        public static void AddATask()
        {
            Console.Write("Enter Task Id : ");
            string ReadTaskId = Console.ReadLine();
            Console.Write("Enter task description : ");
            string ReadTaskDescription = Console.ReadLine();
            Console.Write("Enter Difficulty (as an integer 1-5) : ");
            string ReadStringDifficulty = Console.ReadLine();
            int ReadDifficulty = 0;
            try { ReadDifficulty = Convert.ToInt32(ReadStringDifficulty); }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Entered difficulty is not an integer, please enter 1-5 {0}", ex.Message);
                return;
            }
            Console.Write("Assigned to : ");
            string ReadAssignedTo = Console.ReadLine();
            Console.Write("Due Date : ");
            string ReadDueDate = Console.ReadLine();
            DateTime DueDate = DateTime.Now;
            try
            {
                DueDate = Convert.ToDateTime(ReadDueDate);
            }
            catch (Exception ex2)
            {
                Console.WriteLine("Error: Entered due date is not a valid date {0}", ex2.Message);
                return;
            }
        }
        public static void RemoveATask()
        {
            Console.WriteLine("Removing a task");
            Console.Write("Enter Task Id : ");
            string ReadTaskId = Console.ReadLine();

        }
        public static void ListTasksByDueDate()
        {
            ToDo.CompareTo((x, y) => x.Datetime.CompareTo(y.Datetime));



        }
        public static void ListTasksByDifficulty()
        {

        }
    }
}
