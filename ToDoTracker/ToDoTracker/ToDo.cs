﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ToDoTracker
{
    public class ToDo
    {
        public string TaskID { get; set; }
        public string Description { get; set; }
        public int Difficulty { get; set; }
        public string AssignedTo { get; set; }
        public DateTime DueDate { get; set; }



        public ToDo(string TaskID, string Description, int Difficulty, string AssignedTo, DateTime DueDate)
        {
            this.TaskID = TaskID;
            this.Description = Description;
            this.Difficulty = Difficulty;
            this.AssignedTo = AssignedTo;
            this.DueDate = DueDate;
        }

        public override string ToString()
        {
            return ("TaskID: " + this.TaskID + " Description: " + this.Description
                + " Difficulty: " + this.Difficulty.ToString() + " Assigned to Name: "
                + this.AssignedTo + " DueDate: " + this.DueDate.ToShortDateString());
        }

        public int CompareTo(ToDo other)
        {
            return DueDate.CompareTo(other.DueDate);
        }

        internal static void Sort(Func<object, object, int> value)
        {
            throw new NotImplementedException();
        }

        public class ListTasksByDifficulty : IComparer<ToDo>
        {
            public int Compare(ToDo x, ToDo y)
            {
                return y.TaskID - x.TaskID;
            }
        }


        internal static void CompareTo(Func<object, object, int> value)
        {
            throw new NotImplementedException();
        }
    }
}
