﻿using ClassPlay;
public class Program
{

    static void Main(string[] args)
    {


        Employee emp2 = new Employee("Jerry", "Taco", 1);
        /*DisplayEmployee(emp2);*/
        emp2.Display();

        Employee emp3 = new Employee("Michael", "Manager", 2);
        emp3.Display();


        Employee emp1 = new Employee("Cody", "Allison", 1, 2016, "IT", "Senior Engineer", 500000);
  
        emp1.Display();


        emp3.Promote("Hiring Manager", 500000);
        emp3.Display();
    }
}
