﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassPlay
{
    public class Employee
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int employeeID { get; set; }
        public int yearJoined { get; set; }
        public string jobTitle { get; set; }
        public string department { get; set; }
        public decimal salary { get; set; }

        public Employee(string firstName, string lastName, int employeeID, int yearJoined, string jobTitle, string department, decimal salary)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.employeeID = employeeID;
            this.yearJoined = yearJoined;
            this.jobTitle = jobTitle;
            this.department = department;
            this.salary = salary;
        }
        public Employee(string firstName, string lastName, int employeeId) : this()
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.employeeID = employeeId;
         
        }

        public Employee()
        {
            this.yearJoined = DateTime.Now.Year;
            this.jobTitle = "New Hire";
            this.department = "TBD";
            this.salary = 31200;

        }

        //methods 
        public void Promote(string newJobTitle, decimal newSalary)
        {
            this.jobTitle = newJobTitle;
            this.salary = newSalary;
        }
        public void Display()
        {
            Console.WriteLine(ToString());
        }

        public override string ToString()
        {
            return "FirstName: " + "  " + this.firstName + "  " + "LastName: " + "  " + this.lastName + "  " +  "Employee.ID: " + "  " + this.employeeID
                + "  " + "hireYear: " + "  " + this.yearJoined  + "jobTitle: " + "  " +  this.jobTitle + "  " + " department: " + "  " +  this.department
                + "  " + "salary: " + "  " + this.salary;
        }
    }


}
