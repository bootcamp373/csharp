﻿class Program
{
    static void Main(string[] args)
    {
        string command;
        do
        {
            Console.WriteLine("CtoF  -  FtoC  -  Quit");
            Console.Write("What would you like to do? ");
            command = Console.ReadLine();
            if (command == "FtoC")
            {

                Console.Write("Please enter the temperature in Fahrenheit: ");

                double deg_in_far = Convert.ToDouble(Console.ReadLine());
                double deg_in_cel = FtoC(deg_in_far);
                Console.WriteLine("Celsius = {0:#,##0.0;(#,##0.0)}", deg_in_cel);
            }
            if (command == "CtoF")
            {

                Console.Write("Please enter the temperature in Celsius: ");

                double deg_in_cel = Convert.ToDouble(Console.ReadLine());
                double deg_in_far = CtoF(deg_in_cel);
                Console.WriteLine("Fahrenheit = {0:#,##0.0;(#,##0.0)}", deg_in_far);
            }
            else if (command != "Quit")
            {
                Console.WriteLine("**Error: unrecognized command");
            }
        } while (command != "Quit");

    }
    static double FtoC(double deg_in_far)
    {
        return (double)(deg_in_far - 32) * (5.0 / 9.0);
    }
    static double CtoF(double deg_in_cel)
    {
        return (double)((deg_in_cel * 9.0) / 5.0) + 32.0;
    }

}