﻿
 Console.WriteLine("Hello, World!");

namespace NameParser2
{

    internal class Program
    {
        static void Main(string[] args)
        {
        }
        public static void GetName(out string fullName, out string firstName, out string lastName, out string middleName)
        {
            Console.Write("Enter a full name: ");
            fullName = Console.ReadLine();
            fullName = fullName.ToUpper();
            firstName = fullName.Substring(0, fullName.IndexOf(" "));

            lastName = fullName.Substring(fullName.LastIndexOf(" "));
            if (fullName.IndexOf(" ") == fullName.LastIndexOf(" "))
            {
                middleName = "";
            }
            else
            {
                middleName = fullName.Substring(fullName.IndexOf(" ") + 1, fullName.LastIndexOf(" ") - fullName.IndexOf(" "));
            };
        }
        }
    }
